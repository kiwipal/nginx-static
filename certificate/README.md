# SSL Certificate

The certificate are declared in the NGINX conf file : `/etc/nginx/conf.d/server/ssl_certificates.conf`

When building images, the docker script (entrypoint.sh) will create an autosigned certificate if these files are missing in the `/home/certificate` folder.

- certificate.crt
- certificate.key

> The autosigned certificate will issue a warning on local environment and are only useful for dev env.

## Production mode

When creating the image for prod, the certificate in `/home/certificate` will be used, so remember to replace the files in the `/home/certificate` folder before builfing the image.
