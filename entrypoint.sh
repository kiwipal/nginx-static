#!/bin/sh

# Create auto-signed SSL certificate if missing ones
if [ ! -f /home/certificate/certificate.crt ]; then
    openssl req -x509 -nodes -days 365 -subj  "/C=CA/ST=QC/O=Kiwipal/CN=nginx-static.com" -newkey rsa:2048 -keyout /home/certificate/certificate.key -out /home/certificate/certificate.crt
fi

# Run NGINX
nginx -g 'daemon off;'