# NGINX Static by Kiwipal

**An NGINX server to serve static files only.**  

Build will copy the files and the image can be runned without volume for production.  
For dev purposes, run with volumes.

## Build
$ docker build -t kiwipal/nginx-static:latest .

## Run
$ docker run -itd -p 4000:80 -p 4001:443 --name nginx-static kiwipal/nginx-static:latest

## Run with volumes to update conf and files
$ docker run -itd -p 4000:80 -p 4001:443 --name nginx-static \
    -v /Users/guillaumelenistour/Documents/www/nginx-static/conf/nginx.conf:/etc/nginx/nginx.conf \
    -v /Users/guillaumelenistour/Documents/www/nginx-static/conf/conf.d:/etc/nginx/conf.d \
    -v /Users/guillaumelenistour/Documents/www/nginx-static/www:/home/www \
    -v /Users/guillaumelenistour/Documents/www/nginx-static/certificate:/home/certificate \
    -v /Users/guillaumelenistour/Documents/www/nginx-static/log:/var/log/nginx \
    -v /Users/guillaumelenistour/Documents/www/nginx-static/error_pages:/home/error_pages \
    kiwipal/nginx-static:latest

## Acess in browser

Access can be done using:  
https://localhost:4001

Or with the domain `nginx-static.com` (to be added in the hosts file on the machine).  
https://nginx-static.com:4001

> The domaine name can be searched/replaced in the code without problem.  

## Useful commands (when loggued on server)

### Display NGINX conf
$ nginx -T

### Restart NGINX
$ nginx -s reload

## SSL Certificate

The certificate are declared in the NGINX conf file : `/etc/nginx/conf.d/server/ssl_certificates.conf`

When building images, the docker script (entrypoint.sh) will create an autosigned certificate if these files are missing in the `/home/certificate` folder.

- certificate.crt
- certificate.key

> The autosigned certificate will issue a warning on local environment and are only useful for dev env.

## Production mode

When creating the image for prod, the certificate in `/home/certificate` will be used, so remember to replace the files in the `/home/certificate` folder before builfing the image.
