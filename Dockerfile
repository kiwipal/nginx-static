FROM alpine

# UPDATE APK
RUN apk update

# INSTALL OPEN SSL
RUN apk add openssl curl ca-certificates

# CONFIGURE CERTIFICATE TO USED STABLE NGINX PACKAGES
RUN printf "%s%s%s\n" \
    "http://nginx.org/packages/alpine/v" \
    `egrep -o '^[0-9]+\.[0-9]+' /etc/alpine-release` \
    "/main" \
    | tee -a /etc/apk/repositories

# CREATE NGINX CERTIFICATES
RUN curl -o /tmp/nginx_signing.rsa.pub https://nginx.org/keys/nginx_signing.rsa.pub
RUN openssl rsa -pubin -in /tmp/nginx_signing.rsa.pub -text -noout
RUN mv /tmp/nginx_signing.rsa.pub /etc/apk/keys/

# INSTALL NGINX
RUN apk add nginx

# COPY NGINX CONF
COPY conf/nginx.conf /etc/nginx/nginx.conf
COPY conf/conf.d/. /etc/nginx/conf.d/

# Copy entrypoint.sh
RUN mkdir -p /home/entrypoint/
COPY ./entrypoint.sh /home/entrypoint/.
RUN chmod 777 -R /home/entrypoint

# COPY SSL CERTIFICATES
RUN mkdir -p /etc/nginx/certificate/
COPY certificate/. /home/certificate/

# COPY ERROR PAGES
RUN mkdir -p /home/error_pages/
COPY error_pages/. /home/error_pages/
RUN chmod 777 -R /home/error_pages

# COPY WWW
COPY www/. /home/www/

EXPOSE 443

STOPSIGNAL SIGTERM

# Run entrypoint.sh
ENTRYPOINT ["/home/entrypoint/entrypoint.sh"]